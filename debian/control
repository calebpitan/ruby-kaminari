Source: ruby-kaminari
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>,
           Utkarsh Gupta <utkarsh@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby-actionview (>= 4.1.0),
               ruby-activerecord (>= 4.1.0),
               ruby-activesupport (>= 4.1.0),
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-kaminari.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-kaminari
Homepage: https://github.com/kaminari/kaminari
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-kaminari
Architecture: all
X-DhRuby-Root: ./
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
Description: pagination engine plugin for Rails 3+ and other modern frameworks
 Kaminari is a Scope & Engine based, clean, powerful, agnostic, customizable
 and sophisticated paginator for Rails 3+
 .
 Features:
  * Clean
  * Easy to use
  * Simple scope-based API
  * Customizable engine-based I18n-aware helper
  * ORM & template engine agnostic
  * Modern
 .
 Learn more at /usr/share/doc/ruby-kaminari/README.rdoc

Package: ruby-kaminari-core
Architecture: all
X-DhRuby-Root: kaminari-core/
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
Description: Kaminari's core pagination library
 kaminari-core includes pagination logic independent from ORMs and view
 libraries.
 .
 Kaminari is a Scope & Engine based, clean, powerful, agnostic, customizable
 and sophisticated paginator for Rails 3+

Package: ruby-kaminari-actionview
Architecture: all
X-DhRuby-Root: kaminari-actionview/
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
Description: Kaminari Action View adapter
 kaminari-actionview provides pagination helpers for your Action View templates
 .
 Kaminari is a Scope & Engine based, clean, powerful, agnostic, customizable
 and sophisticated paginator for Rails 3+

Package: ruby-kaminari-activerecord
Architecture: all
X-DhRuby-Root: kaminari-activerecord/
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
Description: Kaminari Active Record adapter
 kaminari-activerecord lets your Active Record models be paginatable
 .
 Kaminari is a Scope & Engine based, clean, powerful, agnostic, customizable
 and sophisticated paginator for Rails 3+
